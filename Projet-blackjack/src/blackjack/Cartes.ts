export interface classeCarte {
  nom: string;
  type: string;
  Image: string;
  valeur1: number;
  valeur2: number;
}

const Paquet: classeCarte[] = [
  {
    nom: "As de coeur",
    type: "As",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/01_of_hearts_A.svg/1200px-01_of_hearts_A.svg.png",
    valeur1: 11,
    valeur2: 1,
  },
  {
    nom: "As de pique",
    type: "As",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ab/01_of_spades_A.svg/1200px-01_of_spades_A.svg.png",
    valeur1: 11,
    valeur2: 1,
  },
  {
    nom: "As de carreau",
    type: "As",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/21/01_of_diamonds_A.svg/langfr-800px-01_of_diamonds_A.svg.png",
    valeur1: 11,
    valeur2: 1,
  },
  {
    nom: "As de trèfle",
    type: "As",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/01_of_clubs_A.svg/1200px-01_of_clubs_A.svg.png",
    valeur1: 11,
    valeur2: 1,
  },
  {
    nom: "Deux de coeur",
    type: "Deux",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/74/02_of_hearts.svg/1200px-02_of_hearts.svg.png",
    valeur1: 2,
    valeur2: 2,
  },
  {
    nom: "Deux de pique",
    type: "Deux",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/40/02_of_spades.svg/1200px-02_of_spades.svg.png",
    valeur1: 2,
    valeur2: 2,
  },
  {
    nom: "Deux de carreau",
    type: "Deux",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ea/02_of_diamonds.svg/1200px-02_of_diamonds.svg.png",
    valeur1: 2,
    valeur2: 2,
  },
  {
    nom: "Deux de trèfle",
    type: "Deux",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d7/02_of_clubs.svg/1200px-02_of_clubs.svg.png",
    valeur1: 2,
    valeur2: 2,
  },
  {
    nom: "Trois de coeur",
    type: "Trois",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/03_of_hearts.svg/1200px-03_of_hearts.svg.png",
    valeur1: 3,
    valeur2: 3,
  },
  {
    nom: "Trois de pique",
    type: "Trois",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/03_of_spades.svg/1200px-03_of_spades.svg.png",
    valeur1: 3,
    valeur2: 3,
  },
  {
    nom: "Trois de carreau",
    type: "Trois",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/00/03_of_diamonds.svg/1200px-03_of_diamonds.svg.png",
    valeur1: 3,
    valeur2: 3,
  },
  {
    nom: "Trois de trèfle",
    type: "Trois",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/03_of_clubs.svg/1200px-03_of_clubs.svg.png",
    valeur1: 3,
    valeur2: 3,
  },
  {
    nom: "Quatre de coeur",
    type: "Quatre",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/26/04_of_hearts.svg/1200px-04_of_hearts.svg.png",
    valeur1: 4,
    valeur2: 4,
  },
  {
    nom: "Quatre de pique",
    type: "Quatre",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7a/04_of_spades.svg/1200px-04_of_spades.svg.png",
    valeur1: 4,
    valeur2: 4,
  },
  {
    nom: "Quatre de carreau",
    type: "Quatre",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/04_of_diamonds.svg/1200px-04_of_diamonds.svg.png",
    valeur1: 4,
    valeur2: 4,
  },
  {
    nom: "Quatre de trèfle",
    type: "Quatre",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ab/04_of_clubs.svg/1200px-04_of_clubs.svg.png",
    valeur1: 4,
    valeur2: 4,
  },
  {
    nom: "Cinq de coeur",
    type: "Cinq",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/05_of_hearts.svg/1200px-05_of_hearts.svg.png",
    valeur1: 5,
    valeur2: 5,
  },
  {
    nom: "Cinq de pique",
    type: "Cinq",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/05_of_spades.svg/1200px-05_of_spades.svg.png",
    valeur1: 5,
    valeur2: 5,
  },
  {
    nom: "Cinq de carreau",
    type: "Cinq",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2e/05_of_diamonds.svg/1200px-05_of_diamonds.svg.png",
    valeur1: 5,
    valeur2: 5,
  },
  {
    nom: "Cinq de trèfle",
    type: "Cinq",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/05_of_clubs.svg/1200px-05_of_clubs.svg.png",
    valeur1: 5,
    valeur2: 5,
  },
  {
    nom: "Six de coeur",
    type: "Six",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0c/06_of_hearts.svg/1200px-06_of_hearts.svg.png",
    valeur1: 6,
    valeur2: 6,
  },
  {
    nom: "Six de pique",
    type: "Six",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/06_of_spades.svg/1200px-06_of_spades.svg.png",
    valeur1: 6,
    valeur2: 6,
  },
  {
    nom: "Six de carreau",
    type: "Six",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c6/06_of_diamonds.svg/langfr-225px-06_of_diamonds.svg.png",
    valeur1: 6,
    valeur2: 6,
  },
  {
    nom: "Six de trèfle",
    type: "Six",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/be/06_of_clubs.svg/langfr-800px-06_of_clubs.svg.png",
    valeur1: 6,
    valeur2: 6,
  },
  {
    nom: "Sept de coeur",
    type: "Sept",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/07_of_hearts.svg/1200px-07_of_hearts.svg.png",
    valeur1: 7,
    valeur2: 7,
  },
  {
    nom: "Sept de pique",
    type: "Sept",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/06/07_of_spades.svg/1200px-07_of_spades.svg.png",
    valeur1: 7,
    valeur2: 7,
  },
  {
    nom: "Sept de carreau",
    type: "Sept",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/07_of_diamonds.svg/1200px-07_of_diamonds.svg.png",
    valeur1: 7,
    valeur2: 7,
  },
  {
    nom: "Sept de trèfle",
    type: "Sept",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/07_of_clubs.svg/1200px-07_of_clubs.svg.png",
    valeur1: 7,
    valeur2: 7,
  },
  {
    nom: "Huit de coeur",
    type: "Huit",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/37/08_of_hearts.svg/1200px-08_of_hearts.svg.png",
    valeur1: 8,
    valeur2: 8,
  },
  {
    nom: "Huit de pique",
    type: "Huit",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/dc/08_of_spades.svg/1200px-08_of_spades.svg.png",
    valeur1: 8,
    valeur2: 8,
  },
  {
    nom: "Huit de carreau",
    type: "Huit",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/08_of_diamonds.svg/1200px-08_of_diamonds.svg.png",
    valeur1: 8,
    valeur2: 8,
  },
  {
    nom: "Huit de trèfle",
    type: "Huit",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/08_of_clubs.svg/1200px-08_of_clubs.svg.png",
    valeur1: 8,
    valeur2: 8,
  },
  {
    nom: "Neuf de coeur",
    type: "Neuf",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e7/09_of_hearts.svg/1200px-09_of_hearts.svg.png",
    valeur1: 9,
    valeur2: 9,
  },
  {
    nom: "Neuf de pique",
    type: "Neuf",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4f/09_of_spades.svg/1200px-09_of_spades.svg.png",
    valeur1: 9,
    valeur2: 9,
  },
  {
    nom: "Neuf de carreau",
    type: "Neuf",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/09_of_diamonds.svg/langfr-150px-09_of_diamonds.svg.png",
    valeur1: 9,
    valeur2: 9,
  },
  {
    nom: "Neuf de trèfle",
    type: "Neuf",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1f/09_of_clubs.svg/1200px-09_of_clubs.svg.png",
    valeur1: 9,
    valeur2: 9,
  },
  {
    nom: "Dix de coeur",
    type: "Dix",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/10_of_hearts_-_David_Bellot.svg/1200px-10_of_hearts_-_David_Bellot.svg.png",
    valeur1: 10,
    valeur2: 10,
  },
  {
    nom: "Dix de pique",
    type: "Dix",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b7/10_of_spades_-_David_Bellot.svg/1200px-10_of_spades_-_David_Bellot.svg.png",
    valeur1: 10,
    valeur2: 10,
  },
  {
    nom: "Dix de carreau",
    type: "Dix",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/10_of_diamonds_-_David_Bellot.svg/1200px-10_of_diamonds_-_David_Bellot.svg.png",
    valeur1: 10,
    valeur2: 10,
  },
  {
    nom: "Dix de trèfle",
    type: "Dix",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bb/10_of_clubs_-_David_Bellot.svg/1200px-10_of_clubs_-_David_Bellot.svg.png",
    valeur1: 10,
    valeur2: 10,
  },
  {
    nom: "Valet de coeur",
    type: "Valet",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/00/Jack_of_hearts_fr.svg/langfr-800px-Jack_of_hearts_fr.svg.png",
    valeur1: 10,
    valeur2: 10,
  },
  {
    nom: "Valet de pique",
    type: "Valet",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/Jack_of_spades_fr.svg/langfr-800px-Jack_of_spades_fr.svg.png",
    valeur1: 10,
    valeur2: 10,
  },
  {
    nom: "Valet de carreau",
    type: "Valet",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/Jack_of_diamonds_fr.svg/langfr-800px-Jack_of_diamonds_fr.svg.png",
    valeur1: 10,
    valeur2: 10,
  },
  {
    nom: "Valet de trèfle",
    type: "Valet",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/df/Jack_of_clubs_fr.svg/langfr-800px-Jack_of_clubs_fr.svg.png",
    valeur1: 10,
    valeur2: 10,
  },
  {
    nom: "Dame de coeur",
    type: "Dame",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/Queen_of_hearts_fr.svg/langfr-800px-Queen_of_hearts_fr.svg.png",
    valeur1: 10,
    valeur2: 10,
  },
  {
    nom: "Dame de pique",
    type: "Dame",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/06/Queen_of_spades_fr.svg/langfr-800px-Queen_of_spades_fr.svg.png",
    valeur1: 10,
    valeur2: 10,
  },
  {
    nom: "Dame de carreau",
    type: "Dame",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/00/Queen_of_diamonds_fr.svg/langfr-800px-Queen_of_diamonds_fr.svg.png",
    valeur1: 10,
    valeur2: 10,
  },
  {
    nom: "Dame de trèfle",
    type: "Dame",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/Queen_of_clubs_fr.svg/langfr-800px-Queen_of_clubs_fr.svg.png",
    valeur1: 10,
    valeur2: 10,
  },
  {
    nom: "Roi de coeur",
    type: "Roi",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/King_of_hearts_fr.svg/langfr-800px-King_of_hearts_fr.svg.png",
    valeur1: 10,
    valeur2: 10,
  },
  {
    nom: "Roi de pique",
    type: "Roi",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/King_of_spades_fr.svg/langfr-800px-King_of_spades_fr.svg.png",
    valeur1: 10,
    valeur2: 10,
  },
  {
    nom: "Roi de carreau",
    type: "Roi",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f7/King_of_diamonds_fr.svg/langfr-800px-King_of_diamonds_fr.svg.png",
    valeur1: 10,
    valeur2: 10,
  },
  {
    nom: "Roi de trèfle",
    type: "Roi",
    Image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/King_of_clubs_fr.svg/langfr-800px-King_of_clubs_fr.svg.png",
    valeur1: 10,
    valeur2: 10,
  },
];

export const PiocherCarte = () => {
  const nb = Math.floor(Math.random() * Math.floor(52));
  return Paquet[nb];
};
