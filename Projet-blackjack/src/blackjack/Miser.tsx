import React from "react";
import { IonInput, IonButton } from "@ionic/react";
import Blackjack from "./Blackjack.png";
import Dollar from "./Dollar.jpg";
import Fond from "./Fond.jpg";
import Jetons from "./Jetons.jpg";
import Stack from "./Stack";
import { recharger } from "./Recharger";
import Underline from "./Underline.png";

const Miser = ({
  onMise,
  stack,
  onmenu,
}: {
  onMise: Function;
  stack: number;
  onmenu: Function;
}) => {
  return (
    <>
      <img src={Jetons} id="Jetons" alt="Jetons"></img>
      <Stack stack={stack} id="StackDébut" />
      <IonInput
        clearInput={true}
        type="number"
        id="mise"
        className="inputmise"
        name="mise"
        min="0"
        max="100"
        placeholder="Montant de la mise"
      ></IonInput>
      <img src={Dollar} id="Dollar1" alt="Dollar"></img>
      <img src={Blackjack} id="Blackjack" alt="Blackjack"></img>
      <img src={Dollar} id="Dollar2" alt="Dollar"></img>
      <img src={Underline} id="Underline" alt="Underline"></img>
      <IonButton
        size="large"
        onClick={() =>
          onMise((document.getElementById("mise") as HTMLInputElement).value)
        }
        id="boutonmise"
      >
        Miser
      </IonButton>
      <img src={Fond} id="Fond" alt="Fond"></img>
      <IonButton onClick={() => onmenu("regles")} id="regles">
        Règles
      </IonButton>
    </>
  );
};

export const MenuMiser = ({
  onstack,
  onMise,
  stack,
  onmenu,
}: {
  onstack: Function;
  onMise: Function;
  stack: number;
  onmenu: Function;
}) => {
  return (
    <>
      {stack === 0 ? (
        <>
          <Miser onMise={onMise} stack={stack} onmenu={onmenu} />
          <IonButton onClick={() => recharger(onstack)} id="recharger">
            Recharger
          </IonButton>
        </>
      ) : (
        <Miser onMise={onMise} stack={stack} onmenu={onmenu} />
      )}
    </>
  );
};
