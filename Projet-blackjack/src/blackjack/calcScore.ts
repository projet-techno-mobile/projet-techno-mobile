import  {classeCarte}  from "./Cartes";

export const calcScore = (main: Array<classeCarte>) => {
  var a = 0;
  main.map((carte) => (a = a + carte.valeur1));
  if (a <= 21) {
    return a;
  } else {
    a = 0;
    var bool = true;
    for (let i = 0; i < main.length; i++) {
      if (main[i].type === "As" && bool === true) {
        a = a + main[i].valeur1;
        bool = false;
      } else {
        a = a + main[i].valeur2;
      }
    }
    if (a <= 21) {
      return a;
    } else {
      a = 0;
      main.map((carte) => (a = a + carte.valeur2));
      return a;
    }
  }
};
