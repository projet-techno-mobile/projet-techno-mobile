import { calcScore } from "./calcScore";
import { PiocherCarte, classeCarte } from "./Cartes";

export const piocheBanque = (mainb: Array<classeCarte>) => {
    const cartes = mainb;
    while (calcScore(cartes) < 17) {
      const nouvcarte = PiocherCarte();
      cartes.push(nouvcarte);
    }
    return cartes;
  };