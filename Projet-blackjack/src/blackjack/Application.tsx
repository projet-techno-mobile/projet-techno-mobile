import React from "react";
import { Jeu } from "./Jeu";
import { IonButton } from "@ionic/react";
import SmileyLuck from "./SmileyLuck.png";

export const Application = () => {
  const [menu, setMenu] = React.useState("jeu");

  const Regles = () => {
    return (
      <>
      <h1>Règles du jeu</h1>
      <ol>
      <li>L'objectif est de se rapprocher le plus possible de 21 sans le dépasser.</li>
      <li>Les valeurs de cartes:
        <ul>
          <li>A: 11pts si le score ne dépasse pas 21pts et 1pts sinon.</li>
          <li>K/Q/J/10: 10pts</li>
          <li>De 2 à 9 leur valeur nominale <br/> (exemple: <img id="cinq" src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2e/05_of_diamonds.svg/1200px-05_of_diamonds.svg.png" alt="cinq"/> vaut 5pts)</li>
        </ul>
      </li>
      <li>Avant chaque manche, vous choisissez la somme que vous voulez mettre en jeu.</li>
      <li>Ensuite, vous et la banque recevez 2 cartes. Cependant, une seule carte de la banque est visible.</li>
      <li>Si la somme de vos deux cartes initiales est égale à 21 <br/>(exemple: <img id="as" src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/ab/01_of_spades_A.svg/1200px-01_of_spades_A.svg.png" alt="as"/> <img id="valet" src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/df/Jack_of_clubs_fr.svg/langfr-800px-Jack_of_clubs_fr.svg.png" alt="valet"/>) <br/>vous gagnez immédiatement 2.5 fois votre mise. </li>
      <li>Sinon, à chaque instant, vous avez la possibilité de piocher une carte ou de vous arrêter.</li>
      <li>Si après avoir pioché une carte, votre score est supérieur à 21, vous perdez immédiatement la somme misée.</li>
      <li>Une fois que vous décidez de vous arrêter, la banque pioche à son tour jusqu'à atteindre un minimum de 17pts.</li>
      <li>Si votre score est supérieur à celui de la banque ou que la banque dépasse 21pts, vous gagnez 2 fois votre mise. Sinon, vous perdez la somme misée.</li>
      <li>Lorsque vous recevez une paire pour main de départ, le bouton "split" apparaît. Celui-ci sera fonctionnel suite aux prochaines mises à jour ! </li>
      <li>Ici, compter les cartes est inutile, en effet, les cartes sont tirées avec remise.</li>
      </ol>
      <div id="amuse">Amusez-vous bien !!!</div>
      <img src={SmileyLuck} id="SmileyLuck" alt="SmileyLuck"></img>
      <IonButton id="Jouer" onClick={() => setMenu("mise")}>Jouer</IonButton>
      </>
    );
  };

  const Menu = () => {
    return (
      <>{menu && menu === "regles" ? <Regles /> : <Jeu onmenu={setMenu} />}</>
    );
  };

  return <Menu />;
};
