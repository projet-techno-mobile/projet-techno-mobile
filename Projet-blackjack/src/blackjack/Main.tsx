import React from "react";
import { classeCarte } from "./Cartes";
import { Carte } from "./Carte";

export const Main = ({
  main,
  classe,
}: {
  main: Array<classeCarte>;
  classe: string;
}) => {
  const cartes = main.map((carte, i) => <Carte carte={carte} key={i} />);
  return (
    <>
      <div id={classe}>{cartes}</div>
    </>
  );
};
