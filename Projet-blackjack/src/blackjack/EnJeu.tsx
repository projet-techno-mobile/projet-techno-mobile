import React from "react";
import { IonButton } from "@ionic/react";
import { piocheBanque } from "./piocheBanque";
import { calcScore } from "./calcScore";
import { classeCarte } from "./Cartes";
import { Main } from "./Main";
import { CarteB } from "./CarteB";
import { Vibration } from "@ionic-native/vibration";

export const EnJeu = ({
  onpioche,
  mainb,
  main,
  onmainbanque,
  finmanche,
  augmentestack,
  mise,
}: {
  onpioche: Function;
  mainb: Array<classeCarte>;
  main: Array<classeCarte>;
  onmainbanque: Function;
  finmanche: Function;
  augmentestack: Function;
  mise: number;
}) => {
  const [etatPartie, setEtatPartie] = React.useState("en partie");

  const Sarreter = () => {
    onmainbanque(piocheBanque(mainb));
    setEtatPartie("fin partie");
  };

  const EnPartie = ({
    main,
    mainb,
  }: {
    main: Array<classeCarte>;
    mainb: Array<classeCarte>;
  }) => {
    const CartePasId = ({ classe }: { classe: string }) => {
      return (
        <>
          <IonButton className={classe} onClick={() => onpioche()}>
            Piocher une carte
          </IonButton>
          <IonButton className={classe} onClick={() => Sarreter()}>
            S'arrêter
          </IonButton>
          <div id="cartedelabanquedebut">Carte de la banque :</div>
          <CarteB carteb={mainb[1]} />
        </>
      );
    };
    return (
      <>
        {main[0].type === main[1].type && main.length === 2 ? (
          <>
            <IonButton className="boutonsenjeusplit">Split</IonButton>
            <CartePasId classe={"boutonsenjeusplit"} />
          </>
        ) : (
          <CartePasId classe={"boutonsenjeu"} />
        )}
      </>
    );
  };

  const Statut = (main: Array<classeCarte>, mainb: Array<classeCarte>) => {
    if (calcScore(mainb) > 21 || calcScore(main) > calcScore(mainb)) {
      return "gagné";
    } else if (calcScore(main) === calcScore(mainb)) {
      return "égalité";
    } else {
      return "perdu";
    }
  };

  const FinPartie = ({ finmanche }: { finmanche: Function }) => {
    const Gain = () => {
      if (Statut(main, mainb) === "gagné") {
        augmentestack(2 * mise);
      } else if (Statut(main, mainb) === "égalité") {
        augmentestack(1 * mise);
      }
      finmanche();
    };

    const Showdown = () => {
      if (Statut(main, mainb) === "gagné") {
        return (
          <>
            {Vibration.vibrate(500)}
            <div id="res">Vous avez gagné {2 * mise}€</div>
          </>
        );
      } else if (Statut(main, mainb) === "égalité") {
        return <div id="res">Vous récupérez votre mise soit {mise}€</div>;
      } else {
        return <div id="res">Tu as perdu ... Essaie encore !!!</div>;
      }
    };
    return (
      <>
        <div id="cartesdelabanqueres">Cartes de la banque :</div>
        <Main classe="mainb" main={mainb} />
        <Showdown />
        <IonButton id="rejouer" onClick={() => Gain()}>
          Rejouer
        </IonButton>
      </>
    );
  };
  const ChangeEtatPartie = () => {
    return (
      <>
        {etatPartie && etatPartie === "en partie" ? (
          <EnPartie main={main} mainb={mainb} />
        ) : (
          <FinPartie finmanche={finmanche} />
        )}
      </>
    );
  };
  return (
    <>
      <ChangeEtatPartie />
    </>
  );
};
