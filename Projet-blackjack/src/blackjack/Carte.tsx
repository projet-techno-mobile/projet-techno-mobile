import React from "react";
import { classeCarte } from "./Cartes";

export const Carte = ({ carte }: { carte: classeCarte }) => {
  return <img src={carte.Image} alt="carte" className="carte" />;
};
