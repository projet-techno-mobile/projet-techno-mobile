import React from "react";
import { IonButton } from "@ionic/react";
import { EnJeu } from "./EnJeu";
import { FinJeu } from "./FinJeu";
import { classeCarte } from "./Cartes";
import { Main } from "./Main";
import smileyBlackjack from "./smileyBlackjack.png";
import { Vibration } from "@ionic-native/vibration";
import Stack from "./Stack";

export const Jouer = ({
  score,
  main,
  mise,
  mainb,
  onpioche,
  finmanche,
  onmainbanque,
  augmentestack,
  stack,
}: {
  score: number;
  main: Array<classeCarte>;
  mise: number;
  mainb: Array<classeCarte>;
  onpioche: Function;
  finmanche: Function;
  onpiochebanque: Function;
  onmainbanque: Function;
  augmentestack: Function;
  stack: number;
}) => {
  const Banqueroute = () => {
    return (
      <>
        {score <= 21 ? (
          <EnJeu
            onpioche={onpioche}
            mainb={mainb}
            main={main}
            onmainbanque={onmainbanque}
            finmanche={finmanche}
            augmentestack={augmentestack}
            mise={mise}
          />
        ) : (
          <FinJeu finmanche={finmanche} />
        )}
      </>
    );
  };

  const GainBlackjack = () => {
    augmentestack(2.5 * mise);
    finmanche();
  };

  const Blackjack = () => {
    return (
      <>
        {Vibration.vibrate([800, 100, 800, 100, 800])}
        <img
          src={smileyBlackjack}
          id="smileyBlackjack"
          alt="smileyBlackjack"
        ></img>
        <div id="blackjack">BLACKJACK !!!</div>
        <div id="gainbj">Vous avez gagné {2.5 * mise}€</div>
        <IonButton id="rejouerbj" onClick={() => GainBlackjack()}>
          Rejouer
        </IonButton>
      </>
    );
  };

  return (
    <>
      <Stack stack={stack} id="StackEnJeu" />
      <div id="voscartes">Vos cartes :</div>
      <Main classe="main" main={main} />
      <div id="score">Votre score : {score}</div>
      <div id="montantmise">Montant misé : {mise}</div>
      {score === 21 && main.length === 2 ? <Blackjack /> : <Banqueroute />}
    </>
  );
};
