import React from "react";
import { IonButton } from "@ionic/react";
import smileyPerdu from "./smileyPerdu.jpg";

export const FinJeu = ({ finmanche }: { finmanche: Function }) => {
  return (
    <>
      <img src={smileyPerdu} id="smileyPerdu" alt="smileyPerdu"></img>
      <div id="bqr">Perdu...</div>
      <IonButton id="rejouerbqr" onClick={() => finmanche()}>
        Rejouer
      </IonButton>
    </>
  );
};
