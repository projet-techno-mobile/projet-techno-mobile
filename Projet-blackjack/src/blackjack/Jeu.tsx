import React from "react";
import { PiocherCarte } from "./Cartes";
import { MenuMiser } from "./Miser";
import { Jouer } from "./Jouer";
import { calcScore } from "./calcScore";

export const Jeu = ({ onmenu }: { onmenu: Function }) => {
  const [stack, setStack] = React.useState(200);
  const [mise, setMise] = React.useState(0);
  const [main, setMain] = React.useState([PiocherCarte(), PiocherCarte()]);
  const [moment, setMoment] = React.useState("mise");

  const [mainbanque, setMainbanque] = React.useState([
    PiocherCarte(),
    PiocherCarte(),
  ]);

  const MainDeDepart = () => {
    setMain([PiocherCarte(), PiocherCarte()]);
    setMainbanque([PiocherCarte(), PiocherCarte()]);
  };
  const piocherUneCarte = () => {
    setMain([...main, PiocherCarte()]);
  };
  const piocherUneCarteBanque = () => {
    setMainbanque([...mainbanque, PiocherCarte()]);
  };

  const augmenteStack = (val: number) => {
    const nouvStack = stack + val;
    setStack(nouvStack);
  };
  const handleMiser = (nombre: number) => {
    if (nombre <= 0 || nombre > stack) {
      return null;
    } else {
      setMise(nombre);
      setStack(stack - nombre);
      setMoment("distcarte");
    }
  };

  const finManche = () => {
    setMoment("mise");
    MainDeDepart();
  };

  const MomentPartie = ({ moment }: { moment: string }) => {
    return (
      <>
        {moment && moment === "mise" ? (
          <MenuMiser
            onMise={handleMiser}
            stack={stack}
            onstack={setStack}
            onmenu={onmenu}
          />
        ) : (
          <Jouer
            score={calcScore(main)}
            main={main}
            mise={mise}
            onpioche={piocherUneCarte}
            mainb={mainbanque}
            finmanche={finManche}
            onpiochebanque={piocherUneCarteBanque}
            onmainbanque={setMainbanque}
            augmentestack={augmenteStack}
            stack={stack}
          />
        )}
      </>
    );
  };
  return (
    <>
      <MomentPartie moment={moment} />
    </>
  );
};
