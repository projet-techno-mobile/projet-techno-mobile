import React from "react";
import { classeCarte } from "./Cartes";

export const CarteB = ({ carteb }: { carteb: classeCarte }) => {
  return <img src={carteb.Image} alt="carteb" className="carteb" />;
};
