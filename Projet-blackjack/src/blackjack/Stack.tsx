import React from "react";

const Stack = ({stack, id } : {stack: number, id: string}) => {
  return <div id={id}>Solde : {stack}€</div>;
};

export default Stack;
